#!groovy
import jenkins.model.*
import hudson.security.*
import jenkins.security.s2m.AdminWhitelistRule


def environ = System.getenv()
def username = environ.containsKey('JENKINS_USERNAME') ? map.JENKINS_ADM_USERNAME : 'centos'
def password = environ.containsKey('JENKINS_PASSWORD') ? map.JENKINS_ADM_USERNAME : 'centos'
def instance = Jenkins.getInstance()


def hudsonRealm = new HudsonPrivateSecurityRealm(false)
hudsonRealm.createAccount(username, password)
instance.setSecurityRealm(hudsonRealm)


def strategy = new FullControlOnceLoggedInAuthorizationStrategy()
instance.setAuthorizationStrategy(strategy)
instance.save()


Jenkins.instance.getInjector().getInstance(AdminWhitelistRule.class).setMasterKillSwitch(false)