# jenkins job builder

## Setup

Setup your credentials in `jenkins-job.ini` file, with the proper username (userid) and password (api token).

## Plugin List

The following command retrieves the list of installed plugins:

```sh
jenkins-jobs --conf jenkins-job.ini get-plugins-info -o plugins_info.yaml
```

## Testing Definitions

The following command will test all job definitions without updating jenkins:

```sh
jenkins-jobs --conf jenkins-job.ini test ./defs
```

## Updating Jobs

```sh
jenkins-jobs --conf jenkins-job.ini update ./defs
```