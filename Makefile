SHELL = /bin/bash
CONF_FILE = ./images/jenkins/jenkins-job.ini
PLUGIN_FILE = ./jenkins/plugins_info.yaml
JOB_DIR = ./defs
IMAGE = quay.io/lrossett/jenkins-centos:latest

.PHONY: jenkins/image/build
jenkins/image/build: jenkins/plugins/convert
	docker build -t ${IMAGE} ./images/jenkins

.PHONY: jenkins/image/debug
jenkins/image/debug:
	docker run -it --ulimit nofile=122880:122880 -m 3G  --entrypoint /bin/bash ${IMAGE}

.PHONY: jenkins/image/run
jenkins/image/run:
	docker run -it -p 8080:8080 -p 50000:50000  --ulimit nofile=122880:122880 -m 3G  ${IMAGE}

.PHONY: jenkins/plugins/get
jenkins/plugins/get:
	jenkins-jobs --conf ${CONF_FILE} get-plugins-info -o ${PLUGIN_FILE}

.PHONY: jenkins/plugins/convert
jenkins/plugins/convert:
	./hack/plugins ${PLUGIN_FILE} > images/jenkins/plugins.txt

.PHONY: jenkins/update
jenkins/update:
	jenkins-jobs --conf ${CONF_FILE} update ${JOB_DIR}
