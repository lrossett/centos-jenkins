# Centos Jenkins

This repository contains Jenkins pipeline code for CentOS Stream automation jobs.

All jobs are configured using the  `jenkins-job-builder` tooling.

## Deployment

Kubernetes deployment files are provided in case oyu want to deploy jenkins itself.

Jenkins-job-builder should work as long as there is a running Jenkins instance with valid credentials.

## Development

A `docker-compose.yml` file is provided for local development.

## License

Licensed under the MIT open-source license.

See [LICENSE](./LICENSE) for details.
